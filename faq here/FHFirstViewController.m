//
//  FHFirstViewController.m
//  FAQ here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHFirstViewController.h"
#import "FHPostCell.h"
#import "FHLoginViewController.h"
#import "FHListViewController.h"

@interface FHFirstViewController ()
- (void) refilter;
@end

@implementation FHFirstViewController
@synthesize near, current, followed, filtered;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Read", @"read");
        self.tabBarItem.image = [UIImage imageNamed:@"first"];
    }
    return self;
}
						
- (void) signOut
{
    UIViewController *loginVC = [FHLoginViewController new];
    
    [self.tabBarController presentModalViewController:loginVC animated:NO];
    [loginVC release];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Sign Out" style:UIBarButtonItemStyleBordered target:self action:@selector(signOut)];
    _tableView.contentOffset = CGPointMake(0.0, 44.0);
    if (self.near) 
    {
        [_tableView reloadData];
    }
	
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[FHNetwork sharedInstance] nearQuestionsWithDelegate:self];
    [[FHNetwork sharedInstance] followedQuestionsWithDelegate:self];
    

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.filtered.count;//(self.current) ? self.followed.count : self.near.count;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 86;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FHPostCell *cell = [_tableView dequeueReusableCellWithIdentifier:@"postCell"];
    if (!cell) 
    {
        cell = [[FHPostCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"postCell"];
    }
    
    NSArray *array = self.filtered;//self.current ? self.followed : self.near;
    NSDictionary *dict = [array objectAtIndex:indexPath.row];
    
    cell.text = [dict objectForKey:@"text"];
    cell.date = [NSDate dateWithTimeIntervalSince1970:[[dict objectForKey:@"time"] intValue]];
    cell.author = [dict objectForKey:@"user_name"];
    cell.postId = [dict objectForKey:@"id"];
    cell.followed = [[dict objectForKey:@"followed"] boolValue];
    cell.followers = [[dict objectForKey:@"followerCount"] intValue];
    if ([[dict objectForKey:@"correctAnswerCount"] intValue] > 0) 
    {
        UIView *bg = [[UIView alloc] initWithFrame:cell.frame];
        bg.backgroundColor = [UIColor greenColor]; //The color you want
        bg.alpha = 0.2;
        cell.backgroundView = bg;
        cell.textLabel.backgroundColor = bg.backgroundColor;
        [bg release];
    }
    
    
    //    NSLog(@"%@", dict);
   
    /*correctAnswerCount = 0;
     followed = 0;
     followerCount = 1;
     id = 19;
     latitude = "50.453189849853516";
     longitude = "30.518013000488280";
     text = "Post 1";
     time = 1335046179;
     "user_id" = 9;
     "user_name" = tyo;
     */
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FHListViewController *list = [[FHListViewController new] autorelease];
    NSArray *array = self.filtered;//self.current ? self.followed : self.near;
    list.question = [array objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:list animated:YES];
}

- (void) followedQuestionsLoaded:(NSArray *)questions
{
    self.followed = questions;
    [self refilter];    
    [_tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
}

- (void) nearQuestionsLoaded:(NSArray *)questions
{
    self.near = questions;
    [self refilter];
    
    [_tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:YES];
}









- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self refilter];
    [_tableView reloadData];
}
// called when text changes (including clear)

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}
// called when keyboard search button pressed

- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar   
{
    
}
// called when bookmark button pressed

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBar
{
    [searchBar resignFirstResponder];
}
// called when cancel button pressed

- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar
{
    
}
// called when search results button pressed

- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope
{
    self.current = selectedScope;
    [self refilter];
    [_tableView reloadData];
}

- (void) refilter
{
    NSArray *array = self.current ? self.followed : self.near;
    
    if (_searchBar.text.length == 0) 
    {
        self.filtered = array;
    }
    else 
    {
        NSMutableArray *newArray = [NSMutableArray array];
        for (NSDictionary *dictionary in array) 
        {
            NSString *str = [[dictionary objectForKey:@"text"] lowercaseString];
            if ([str rangeOfString:[_searchBar.text lowercaseString]].location != NSNotFound) 
            {
                [newArray addObject:dictionary];
            }
        }
        self.filtered = newArray;
    }
}

@end
