//
//  FHCommentCell.m
//  faq here
//
//  Created by Victor Schepanovsky on 22.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHCommentCell.h"

@implementation FHCommentCell
@synthesize textField;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [[[[NSBundle mainBundle] loadNibNamed:@"FHCommentCell" owner:self options:nil] objectAtIndex:0] retain];
    if (self) {
        // Initialization code
    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
