//
//  FHCommentCell.h
//  faq here
//
//  Created by Victor Schepanovsky on 22.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHCommentCell : UITableViewCell
{
//    IBOutlet UITextField *_textField;
}
@property (nonatomic, retain) IBOutlet UITextField *textField;
@end
