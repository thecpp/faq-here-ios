//
//  FHFirstViewController.h
//  FAQ here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHNetwork.h"

@interface FHFirstViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, FHQuestionsDelegate>
{
    NSArray *_nearPosts;
    IBOutlet UITableView *_tableView;
    IBOutlet UISearchBar *_searchBar;
}
@property (nonatomic, retain) NSArray *near;
@property (nonatomic, retain) NSArray *followed;
@property (nonatomic, retain) NSArray *filtered;
@property (nonatomic, assign) NSInteger current;

@end
