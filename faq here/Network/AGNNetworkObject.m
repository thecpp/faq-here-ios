//
//  AGNNetworkObject.m
//  Agnitio iPlanner
//
//  Created by Victor Schepanovsky on 03/12/2011.
//  Copyright (c) 2011 Kuadriga Ltd. All rights reserved.
//

#import "AGNNetworkObject.h"
#import "JSON.h"
//#import "AGNNetworkManager.h"

#define kAGNUserSessionCookiePropertiesKey @"AGNSessionCookieProperties"
static const NSTimeInterval kAGNCommunicationTimeout = 30.0;
static NSHTTPCookie *sSessionCookie;

@interface AGNNetworkObject ()
@property (nonatomic, retain) NSHTTPCookie *sessionCookie;
@property (nonatomic, retain) NSURLConnection *connection;
@property (nonatomic, retain) NSFileHandle *fileHandle;

//- (void) download;
- (void)clearCookies;

@end

@implementation AGNNetworkObject
@synthesize sessionCookie = _sessionCookie;
@synthesize fileHandle = _fileHandle;
@synthesize connection = _connection;

@synthesize address = _address;
@synthesize delegate = _delegate;
@synthesize data = _data;
@synthesize postString= _postString;
@synthesize saveToFileAutomatically = _saveToFileAutomatically;
@synthesize filePath = _filePath;
@synthesize status = _status;
@synthesize expectedSize = _expectedSize;
@synthesize checkFileSize = _checkFileSize;
@synthesize tag = _tag;
@synthesize error = _error;
@synthesize folderPath = _folderPath;
@synthesize remoteFolderPath = _remoteFolderPath;
@synthesize priority = _priority;
@synthesize info = _info;
@synthesize size = _size;

- (NSString *)description {
    return [NSString stringWithFormat:@"<%@: %p, postString: %@, filePath: %@, expectedSize: %d, folderPath: %@, remoteFolderPath: %@, priority: %d size: %d",
                NSStringFromClass([self class]),
            self, _postString, _filePath, _expectedSize, _folderPath, _remoteFolderPath, _priority, _size, nil];
}

- (void) dealloc
{
    if (_queue) 
    {
        dispatch_release(_queue);
    }
   // [self.fileHandle closeFile];
    self.fileHandle = nil;
    self.connection = nil;
    self.postString = nil;
    self.info = nil;
    self.folderPath = nil;
    self.remoteFolderPath = nil;
    self.error = nil;
    self.address = nil;
    self.delegate = nil;
    self.data = nil;
    self.filePath = nil;
    [super dealloc];
}

- (id) initWithPath:(NSString *)path delegate:(id<AGNNetworkDataReceiver>)delegate
{
    
    self = [super init];
    if (self) 
    {
        _retryCount = 3;
        self.address = path;
        self.delegate = delegate;
        //self.data = [NSMutableData data];
        self.status = kAGNNetworkObjectWaiting;
    }
    return self;
}

+ (AGNNetworkObject *)networkObjectWithPath:(NSString *)path delegate:(id<AGNNetworkDataReceiver>)delegate
{
    return [[[AGNNetworkObject alloc] initWithPath:path delegate:delegate] autorelease];
}

- (void) saveToFile
{
    if (self.filePath != nil) 
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:self.filePath]) {
            [self.data writeToFile:self.filePath atomically:NO];
        } else {
             NSString *folder = [self.filePath stringByDeletingLastPathComponent];
            [[NSFileManager defaultManager] createDirectoryAtPath:folder withIntermediateDirectories:YES attributes:nil error:NULL];
            [[NSFileManager defaultManager] createFileAtPath:self.filePath contents:self.data attributes:nil];
        }
    }
}

- (void) setStatus:(AGNNetworkObjectStatus)status
{
    switch (status) 
    {
        case kAGNNetworkObjectDownloaded:
            if (self.checkFileSize && self.data.length != self.expectedSize) 
            {
                _status = kAGNNetworkObjectDownloadFailed;
                NSString *errorDescription = [NSString stringWithFormat:NSLocalizedString(@"Detected size missmatch for file %@. Aborting sync for this presentation.", @"SyncTableViewController"), [self.filePath lastPathComponent]];
                self.error = [NSError errorWithDomain:@"error" code:1 userInfo:[NSDictionary dictionaryWithObject:errorDescription forKey:NSLocalizedDescriptionKey]];
            }
            
            if (self.saveToFileAutomatically) 
            {
                [self saveToFile];
            }
            break;
        default:
            break;
    }
    _status = status;
}

- (void) download
{
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.address] 
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy//NSURLRequestReloadIgnoringLocalCacheData 
                                                       timeoutInterval:kAGNCommunicationTimeout
                                    ];
    
    if (self.postString != nil) 
    {
        NSData *postData = [self.postString dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];    
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];      
        [request setValue:@"application/x-www-form-urlencoded charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    }
    
    /*if (self.tag == kAGNNetworkObjectAuthRequest) 
    {
        [self clearCookies];
    }*/
    
    if (self.sessionCookie)  
    {
        NSArray *cookies = [NSArray arrayWithObjects:_sessionCookie, nil];
        NSDictionary *httpHeaders = [NSHTTPCookie requestHeaderFieldsWithCookies:cookies];
        [request setAllHTTPHeaderFields:httpHeaders];
    }
    
    self.status = kAGNNetworkObjectDownloading;
    
    UIApplication *app = [UIApplication sharedApplication];
    _bgTask = [app beginBackgroundTaskWithExpirationHandler:^{ 
        [app endBackgroundTask:_bgTask]; 
        _bgTask = UIBackgroundTaskInvalid;
    }];
    app.networkActivityIndicatorVisible = YES;
    
    self.size = 0;
    
    if (self.filePath != nil) 
    {                                                                                                                               
        [[NSFileManager defaultManager] createFileAtPath:self.filePath contents:nil attributes:nil];
        @synchronized(self.fileHandle)
        {
            self.fileHandle = [NSFileHandle fileHandleForWritingAtPath:self.filePath];
           // [self.fileHandle seekToEndOfFile];
        }
        self.saveToFileAutomatically = NO;
        self.checkFileSize = NO;
    }
    else
    {
        self.data = [NSMutableData data];
    }
    
    [self performSelectorOnMainThread:@selector(start:) withObject:request waitUntilDone:NO];
    //self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
 //   [self.connection performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:NO];
  //  [self.connection start];
}


- (void) start:(NSURLRequest *)request
{
    self.connection = [NSURLConnection connectionWithRequest:request delegate:self];
}


- (void) setSessionCookie:(NSHTTPCookie *)sessionCookie
{
    if ((!sSessionCookie || ![sSessionCookie isEqual:sessionCookie]) && sessionCookie != nil) 
    {
        [sSessionCookie autorelease];
        sSessionCookie = [sessionCookie retain];
        NSDictionary *sessionCookieProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                                 [sessionCookie domain], NSHTTPCookieDomain,
                                                 [sessionCookie path], NSHTTPCookiePath,
                                                 [sessionCookie name], NSHTTPCookieName,
                                                 [sessionCookie value], NSHTTPCookieValue,
                                                 nil];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:sessionCookieProperties];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:kAGNUserSessionCookiePropertiesKey];
    }
    if (sessionCookie == nil) 
    {
        [sSessionCookie autorelease];
        sSessionCookie = nil;
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kAGNUserSessionCookiePropertiesKey];
    }
}

- (NSHTTPCookie *) sessionCookie
{
    if (!sSessionCookie) 
    {
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:kAGNUserSessionCookiePropertiesKey];
        
        if (data) 
        {
            NSDictionary *sessionCookieProperties = [NSKeyedUnarchiver unarchiveObjectWithData:data];
            sSessionCookie = [[NSHTTPCookie cookieWithProperties:sessionCookieProperties] retain];
        }
    }
    return sSessionCookie;
}

- (void)clearCookies
{
    for (NSHTTPCookie *cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies]) 
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    self.sessionCookie = nil;
}

- (NSURLRequest *)connection:(NSURLConnection *)connection willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse 
{
    return request;
}

- (void)finishWithError:(NSError *)error
{
    if (!self.error && error) 
    {
        self.error = error;
    }
    
    if (self.error && _retryCount) 
    {
        _retryCount--;
        [self performSelectorInBackground:@selector(download) withObject:nil];
    }
    else 
    {
        if (self.delegate != nil) 
        {
            if ([self.delegate respondsToSelector:@selector(requestFinishedWithObject:)]) 
            {
                [(NSObject *)self.delegate performSelectorOnMainThread:@selector(requestFinishedWithObject:) withObject:self waitUntilDone:NO];
            }
        }
    }
}



- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{

    // schepanovsky : block for background downloading in when sleep of in tray.
    UIApplication *app = [UIApplication sharedApplication];
    if (_bgTask != UIBackgroundTaskInvalid) 
    {
        [app endBackgroundTask:_bgTask]; 
        _bgTask = UIBackgroundTaskInvalid;
    }
    app.networkActivityIndicatorVisible = NO;
    // schepanovsky : block end
    if (_canceled) return;
    
    [self.fileHandle closeFile];
    
    self.status = kAGNNetworkObjectDownloaded;
    
    [self finishWithError:self.error];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    if (_canceled) return;

    self.size += data.length;
    
    if(!_queue)
    {
        _queue = dispatch_queue_create("queueForFileSaving", NULL);
    }
    
    dispatch_sync(_queue, ^{
    if (self.filePath) 
        {
            @synchronized(self.fileHandle)
            {
                [self.fileHandle seekToEndOfFile];
                [self.fileHandle writeData:data];
            }
            //            [self.fileHandle synchronizeFile];
        }
        else 
        {
            [self.data appendData:data];
        }
    });
    if (self.delegate != nil && [self.delegate respondsToSelector:@selector(receivedBytesForObject:bytes:)]) 
    {
        [self.delegate receivedBytesForObject:self bytes:data.length];
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    if (_canceled) return;
    self.status = kAGNNetworkObjectDownloadFailed;
    [self finishWithError:error];
}

- (void)connection:(NSURLConnection *)conn didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)response;
   //ALog(@"Connection didReceiveResponse %d %@ ",httpResponse.statusCode, httpResponse.allHeaderFields);
    
    if (httpResponse.statusCode == 200) 
    {
        // Got a response so extract any cookies.  The array will be empty if there are none.
     //   NSDictionary *theHeaders = [httpResponse allHeaderFields];
       // NSArray *theCookies = [NSHTTPCookie cookiesWithResponseHeaderFields:theHeaders forURL:[response URL]];
        
        /*NSHTTPCookie *cookie;
        for (cookie in theCookies)
        {
            if ([[cookie name] isEqualToString:kCookieName]) 
            {
                self.sessionCookie = cookie;
#ifndef NEW_AGNITIO
                
                [Utils saveCurrentUser];
#endif
                break;
            }
        }*/
    } 
    else if (httpResponse.statusCode >= 400 && httpResponse.statusCode < 500) 
    {
        self.status = kAGNNetworkObjectDownloadFailed;
        NSString *errorDescription = [NSString stringWithFormat:NSLocalizedString(@"HTTP client error '%d' occurred.", @"LoginViewController"), httpResponse.statusCode];
        self.error = [NSError errorWithDomain:@"error" code:2 userInfo:[NSDictionary dictionaryWithObject:errorDescription forKey:NSLocalizedDescriptionKey]];
        self.status = kAGNNetworkObjectDownloadFailed;
        [self finishWithError:nil];
    } 
    else if (httpResponse.statusCode >= 500 && httpResponse.statusCode < 600) 
    {
        self.status = kAGNNetworkObjectDownloadFailed;
        NSString *errorDescription = [NSString stringWithFormat:NSLocalizedString(@"HTTP server error '%d' occurred.", @"LoginViewController"), httpResponse.statusCode];
        self.error = [NSError errorWithDomain:@"error" code:3 userInfo:[NSDictionary dictionaryWithObject:errorDescription forKey:NSLocalizedDescriptionKey]];
        self.status = kAGNNetworkObjectDownloadFailed;
        [self finishWithError:nil];
    }
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
	[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];	
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void) cancel
{
    _canceled = YES;
    [self.connection performSelectorOnMainThread:@selector(cancel) withObject:nil waitUntilDone:NO];
}

- (id) JSONValue
{
    return [[[[NSString alloc] initWithData:self.data encoding:NSUTF8StringEncoding] autorelease] JSONValue];
}


@end
