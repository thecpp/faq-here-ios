//
//  FHNetwork.h
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FHComment.h"
#import "AGNNetworkObject.h"

#define FHServerAddress @"http://faq-here.t.proxylocal.com/"

@protocol FHPostDelegate <NSObject>

- (void) postedWithError:(NSError *)error;

@end

@protocol FHQuestionsDelegate <NSObject>

- (void) followedQuestionsLoaded:(NSArray *)questions;
- (void) nearQuestionsLoaded:(NSArray *)questions;

@end

@protocol FHAnswersDelegate <NSObject>

- (void) answersLoaded:(NSArray *)answers;

@end


@interface FHNetwork : NSObject <AGNNetworkDataReceiver>
{
    id<FHPostDelegate> _postDelegate;
    id<FHPostDelegate> _commentDelegate;
    id<FHQuestionsDelegate> _questionsDelegate;
    id<FHAnswersDelegate> _answersDelegate;
}

+ (FHNetwork *) sharedInstance;

- (void) signInWithName:(NSString *)name;

- (void) postQuestion:(NSString *)string delegate:(id<FHPostDelegate>)delegate;
- (void) postAnswer:(NSString *)string forQuestion:(NSString *)questionId delegate:(id<FHPostDelegate>)delegate;

- (void) nearQuestionsWithDelegate:(id<FHQuestionsDelegate>)delegate;
- (void) followedQuestionsWithDelegate:(id<FHQuestionsDelegate>)delegate;
- (void) answersForQuestion:(NSString *)questionId delegate:(id<FHAnswersDelegate>)delegate;

- (void) followQuestion:(NSString *)questionId;
- (void) unFollowQuestion:(NSString *)questionId;

- (void) voteForAnswerWithId:(NSString *)answerId;
- (void) unVoteForAnswerWithId:(NSString *)answerId;

- (void) markAnswerAsRight:(NSString *)answerId;
- (void) unMarkAnswerAsRight:(NSString *)answerId;

@end
