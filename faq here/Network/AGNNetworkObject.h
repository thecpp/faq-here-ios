//
//  AGNNetworkObject.h
//  Agnitio iPlanner
//
//  Created by Victor Schepanovsky on 03/12/2011.
//  Copyright (c) 2011 Kuadriga Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
//#import "AGNNetworkBase.h"

typedef enum 
{
    kAGNNetworkObjectDownloaded = 1,
    kAGNNetworkObjectWaiting = 2,
    kAGNNetworkObjectDownloading = 3,
    kAGNNetworkObjectDownloadFailed = 4
} AGNNetworkObjectStatus;

typedef enum 
{
    kAGNNetworkPriorityLow = -1,
    kAGNNetworkPriorityNormal = 0,
    kAGNNetworkPriorityHigh = 1

} AGNNetworkObjectPriority;


typedef NSInteger AGNNetworkObjectTag;

@class AGNNetworkObject;

@protocol AGNNetworkDataReceiver<NSObject>

@required
- (void) requestFinishedWithObject:(AGNNetworkObject *)object; 

@optional

- (void) receivedBytesForObject:(AGNNetworkObject *)object bytes:(NSUInteger)bytes;

@end

@interface AGNNetworkObject : NSObject <NSURLConnectionDataDelegate>
{
    NSString *_address;
    id<AGNNetworkDataReceiver> _delegate;
    NSMutableData *_data;
    NSString *_postString;
    
    BOOL _saveToFileAutomatically;
    NSString *_filePath;
    
    AGNNetworkObjectStatus _status;
    AGNNetworkObjectPriority _priority;
    
    NSInteger _expectedSize;
    BOOL _checkFileSize;
    
    AGNNetworkObjectTag _tag;
    NSError *_error;
    
    NSString *_folderPath;
    NSString *_remoteFolderPath;
    id _info;
    NSUInteger _size;

    UIBackgroundTaskIdentifier _bgTask;   
    NSURLConnection *_connection;
    NSFileHandle *_fileHandle;
    dispatch_queue_t _queue;
    NSInteger _retryCount; 
    BOOL _canceled;
}
@property (nonatomic, assign) NSUInteger size;
@property (nonatomic, retain) NSString *folderPath;
@property (nonatomic, retain) NSString *remoteFolderPath;
@property (nonatomic, retain) NSError *error;
@property (nonatomic, assign) AGNNetworkObjectTag tag;
@property (nonatomic, assign) AGNNetworkObjectStatus status;
@property (nonatomic, assign) AGNNetworkObjectPriority priority;
@property (nonatomic, retain) NSString *filePath;
@property (nonatomic, assign) BOOL saveToFileAutomatically;
@property (nonatomic, retain) NSString *postString;
@property (nonatomic, retain) NSMutableData *data;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, assign) id<AGNNetworkDataReceiver> delegate;
@property (nonatomic, assign) NSInteger expectedSize;
@property (nonatomic, assign) BOOL checkFileSize;
@property (nonatomic, retain) id info;

- (id) initWithPath:(NSString *)path delegate:(id<AGNNetworkDataReceiver>)delegate;
+ (AGNNetworkObject *)networkObjectWithPath:(NSString *)path delegate:(id<AGNNetworkDataReceiver>)delegate;
- (void) saveToFile;
- (void) download;
- (void) cancel;
- (id) JSONValue;

@end
