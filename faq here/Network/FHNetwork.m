//
//  FHNetwork.m
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHNetwork.h"
#import "FHSettings.h"

#define fhLoginTag 0
#define fhPostTag 1
#define fhAnswerTag 2
#define fhNearListTag 3
#define fhFollowingTag 4
#define fhAnswerList 5
#define fhFollowTag 6

static FHNetwork *sNetwork;
@implementation FHNetwork

+ (FHNetwork *) sharedInstance
{
    if (!sNetwork) 
    {
        sNetwork = [FHNetwork new];
    }
    return sNetwork;
}

- (void) requestFinishedWithObject:(AGNNetworkObject *)object
{
    if (object.tag == fhLoginTag) 
    {
        NSDictionary *result = (NSDictionary *)[object JSONValue]; 
        [FHSettings settings].userId = [[result objectForKey:@"id"] intValue];
        [FHSettings settings].userName = [result objectForKey:@"name"];
    } 
    else if (object.tag == fhPostTag) 
    {
        [_postDelegate postedWithError:object.error];        
    }
    else if (object.tag == fhAnswerTag) 
    {
        [_commentDelegate postedWithError:object.error];        
    }

    else if (object.tag == fhNearListTag) 
    {
        NSArray *result = (NSArray *)[object JSONValue]; 
        [_questionsDelegate nearQuestionsLoaded:result];
    }
    else if (object.tag == fhFollowingTag) 
    {
        NSArray *result = (NSArray *)[object JSONValue]; 
        [_questionsDelegate followedQuestionsLoaded:result];
    }
    else if (object.tag == fhAnswerList)
    {
        NSArray *result = (NSArray *)[object JSONValue]; 
        [_answersDelegate answersLoaded:result];
    }
    else if (object.tag == fhFollowTag)
    {
        NSLog(@"%@", [object JSONValue]);
    }

}



- (void) signInWithName:(NSString *)name
{
    NSString *path = [NSString stringWithFormat:@"%@?r=api/user/logIn&name=%@", FHServerAddress, name];
    //NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhLoginTag;
    [object download];
}


- (void) postQuestion:(NSString *)string delegate:(id<FHPostDelegate>)delegate
{
    _postDelegate = delegate; 
    NSString *path = [NSString stringWithFormat:@"%@?r=api/question/post&text=%@&user_id=%d&lat=%.19f&long=%.19f", FHServerAddress, string, [FHSettings settings].userId, [FHSettings settings].latitude, [FHSettings settings].longtitude];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhPostTag;
    [object download];
}

- (void) postAnswer:(NSString *)string forQuestion:(NSString *)questionId delegate:(id<FHPostDelegate>)delegate
{
    //http://faq-here.t.proxylocal.com/?r=api/answer/post&user_id=1&question_id=1&text=Here
    _commentDelegate = delegate; 
    NSString *path = [NSString stringWithFormat:@"%@?r=api/answer/post&text=%@&user_id=%d&question_id=%@", FHServerAddress, string, [FHSettings settings].userId, questionId];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhAnswerTag;
    [object download];
}

- (void) nearQuestionsWithDelegate:(id<FHQuestionsDelegate>)delegate
{
    _questionsDelegate = delegate; 
    NSString *path = [NSString stringWithFormat:@"%@?r=api/question/list&user_id=%d&lat=%.19f&long=%.19f", FHServerAddress, [FHSettings settings].userId,[FHSettings settings].latitude, [FHSettings settings].longtitude];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhNearListTag;
    [object download];

}

- (void) followedQuestionsWithDelegate:(id<FHQuestionsDelegate>)delegate
{
    _questionsDelegate = delegate; //http://faq-here.t.proxylocal.com/?r=api/question/list&long=0&lat=0&user_id=1
    NSString *path = [NSString stringWithFormat:@"%@?r=api/question/following&user_id=%d", FHServerAddress, [FHSettings settings].userId,[FHSettings settings].latitude, [FHSettings settings].longtitude];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    //NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhFollowingTag;
    [object download];
}

- (void) answersForQuestion:(NSString *)questionId delegate:(id<FHAnswersDelegate>)delegate
{
    _answersDelegate = delegate; //http://faq-here.t.proxylocal.com/?r=api/answer/list&user_id=1&question_id=1
    NSString *path = [NSString stringWithFormat:@"%@?r=api/answer/list&user_id=%d&question_id=%@", FHServerAddress, [FHSettings settings].userId,questionId];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhAnswerList;
    [object download];
}


//http://faq-here.t.proxylocal.com/?r=api/question/follow&user_id=1&question_id=2

- (void) followQuestion:(NSString *)questionId
{
    NSString *path = [NSString stringWithFormat:@"%@?r=api/question/follow&user_id=%d&question_id=%@", FHServerAddress, [FHSettings settings].userId,questionId];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhFollowTag;
    [object download];
}

- (void) unFollowQuestion:(NSString *)questionId
{
    NSString *path = [NSString stringWithFormat:@"%@?r=api/question/unFollow&user_id=%d&question_id=%@", FHServerAddress, [FHSettings settings].userId,questionId];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
     NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhFollowTag;
    [object download];
}

//http://faq-here.t.proxylocal.com/?r=api/answer/vote&user_id=1&answer_id=1

- (void) voteForAnswerWithId:(NSString *)answerId
{
    NSString *path = [NSString stringWithFormat:@"%@?r=api/answer/vote&user_id=%d&answer_id=%@", FHServerAddress, [FHSettings settings].userId, answerId];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhFollowTag;
    [object download];
}

- (void) unVoteForAnswerWithId:(NSString *)answerId
{
    NSString *path = [NSString stringWithFormat:@"%@?r=api/answer/unVote&user_id=%d&answer_id=%@", FHServerAddress, [FHSettings settings].userId,answerId];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhFollowTag;
    [object download];
}

- (void) markAnswerAsRight:(NSString *)answerId
{
    //http://faq-here.t.proxylocal.com/index.php?r=api/answer/markCorrect&user_id=9&answer_id=23
    NSString *path = [NSString stringWithFormat:@"%@?r=api/answer/markCorrect&user_id=%d&answer_id=%@", FHServerAddress, [FHSettings settings].userId, answerId];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhFollowTag;
    [object download];
}

- (void) unMarkAnswerAsRight:(NSString *)answerId
{
    //http://faq-here.t.proxylocal.com/index.php?r=api/answer/markCorrect&user_id=9&answer_id=23
    NSString *path = [NSString stringWithFormat:@"%@?r=api/answer/unMarkCorrect&user_id=%d&answer_id=%@", FHServerAddress, [FHSettings settings].userId, answerId];
    path = [path stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    // NSLog(@"%@", path);
    AGNNetworkObject *object = [AGNNetworkObject networkObjectWithPath:path delegate:self];
    object.tag = fhFollowTag;
    [object download];
}


@end
