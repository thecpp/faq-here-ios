//
//  FHListViewController.m
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHListViewController.h"
#import "FHPostCell.h"
#import "FHAnswerCell.h"
#import "FHCommentCell.h"
@interface FHListViewController ()

@end

@implementation FHListViewController
@synthesize question, answers;
- (id)init
{
    self = [self initWithNibName:@"FHListViewController" bundle:nil];
    if (self) 
    {
        self.title = @"Answers";
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
    NSString *questionId = [self.question objectForKey:@"id"];
    [[FHNetwork sharedInstance] answersForQuestion:questionId delegate:self];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.answers.count + 2;
}

- (float) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) 
    {
        return 86;
    }
    else if (indexPath.row == 1)
    {
        return 72;
    }
    return 69;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    if (indexPath.row == 0) 
    {
        cell = [_tableView dequeueReusableCellWithIdentifier:@"postCell"];
        if (!cell) 
        {
            cell = [[[FHPostCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"postCell"] autorelease];
        }
        [(FHPostCell *)cell setText:[self.question objectForKey:@"text"]];
        [(FHPostCell *)cell setDate : [NSDate dateWithTimeIntervalSince1970:[[self.question objectForKey:@"time"] intValue]]];
        [(FHPostCell *)cell setAuthor : [self.question objectForKey:@"user_name"]];
        
        [(FHPostCell *)cell setPostId : [self.question objectForKey:@"id"]];
        [(FHPostCell *)cell setFollowed : [[self.question objectForKey:@"followed"] boolValue]];
        [(FHPostCell *)cell setFollowers : [[self.question objectForKey:@"followerCount"] intValue]];
        

        
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    else if (indexPath.row == 1)
    {
        cell = [_tableView dequeueReusableCellWithIdentifier:@"commentCell"];
        if (!cell) 
        {
            cell = [[[FHCommentCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"commentCell"] autorelease];
        }
        [(FHCommentCell *)cell textField].delegate = self;
    }
    else 
    {
        cell = [_tableView dequeueReusableCellWithIdentifier:@"answerCell"];
        if (!cell) 
        {
            cell = [[[FHAnswerCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"answerCell"] autorelease];
            NSDictionary *dict = [self.answers objectAtIndex:indexPath.row - 2];
            NSLog(@"%@", dict);
            [(FHAnswerCell *)cell setText:[dict objectForKey:@"text"]];
            [(FHAnswerCell *)cell setDate : [NSDate dateWithTimeIntervalSince1970:[[dict objectForKey:@"time"] intValue]]];
            [(FHAnswerCell *)cell setAuthor : [dict objectForKey:@"user_name"]];
            [(FHAnswerCell *)cell setAuthorId:[[dict objectForKey:@"user_id"] intValue]];
            [(FHAnswerCell *)cell setAnswerId:[[dict objectForKey:@"id"] intValue]];
            [(FHAnswerCell *)cell setHelpful:[[dict objectForKey:@"helpful"] intValue]];            
            [(FHAnswerCell *)cell setIsCorrect:[[dict objectForKey:@"is_correct"] boolValue]];
            [(FHAnswerCell *)cell setRating:[[dict objectForKey:@"rating"] intValue]];
            [(FHAnswerCell *)cell setPostAuthorId:[[self.question objectForKey:@"user_id"] intValue]];
            
            
            /*helpful = 0;
             id = 44;
             "is_correct" = 0;
             rating = 0;
             text = dddddddqwshbhrf;
             time = 1335052473;
             "user_id" = 1;
             "user_name" = Bogdan;*/
            cell.accessoryType = UITableViewCellAccessoryNone;

        }
    }
    return cell;
}

- (void) answersLoaded:(NSArray *)newanswers
{
    self.answers = newanswers;
    [_tableView reloadData];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [[FHNetwork sharedInstance] postAnswer:textField.text forQuestion:[self.question objectForKey:@"id"] delegate:self];
    return YES;
}

- (void) postedWithError:(NSError *)error
{
    NSString *text = (!error)? @"Successfully added" : @"Failed";
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:text message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];

    
    NSString *questionId = [self.question objectForKey:@"id"];
    [[FHNetwork sharedInstance] answersForQuestion:questionId delegate:self];   
}

@end
