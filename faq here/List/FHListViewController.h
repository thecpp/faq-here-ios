//
//  FHListViewController.h
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHNetwork.h"

@interface FHListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, FHAnswersDelegate,UITextFieldDelegate, FHPostDelegate>
{
    IBOutlet UITableView *_tableView;
}
@property (nonatomic, retain) NSDictionary *question;
@property (nonatomic, retain) NSArray *answers;
@end
