//
//  FHPostCell.m
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHPostCell.h"
#import "FHNetwork.h"
@implementation FHPostCell
@synthesize text, date, author, followed, followers, postId;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [[[[NSBundle mainBundle] loadNibNamed:@"FHPostCell" owner:self options:nil] objectAtIndex:0] retain];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setDate:(NSDate *)newDate
{
    date = newDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    _dateLabel.text = [dateFormatter stringFromDate:newDate];
    [dateFormatter release];
}

- (void) setAuthor:(NSString *)authorText
{
    author = authorText;
    _authorLabel.text = authorText;
}

- (void) setText:(NSString *)textString
{
    text = textString;
    _textView.text = textString;
}

- (void) setFollowed:(BOOL)isfollowed
{
    followed = isfollowed;
    NSString *name = isfollowed ? @"watched-icon-orange":@"watched-icon-black";
    UIImage *img = [UIImage imageNamed:name];
    [_followersButton setImage:img forState:UIControlStateNormal];
    [_followersButton setImage:img forState:UIControlStateSelected];
    [_followersButton setImage:img forState:UIControlStateHighlighted];
}

- (void) setFollowers:(NSInteger)cfollowers
{
    followers = cfollowers;
    NSString *str = [NSString stringWithFormat:@"%d",cfollowers];
    [_followersButton setTitle:str forState:UIControlStateNormal];
    [_followersButton setTitle:str forState:UIControlStateSelected];
    [_followersButton setTitle:str forState:UIControlStateHighlighted];
}


- (IBAction)pressedFollow:(id)sender
{
    if (followed) 
    {
        [[FHNetwork sharedInstance] unFollowQuestion:self.postId];
        self.followers--;
    }
    else 
    {
        [[FHNetwork sharedInstance] followQuestion:self.postId];
        self.followers++;
    }
    self.followed = !followed;
}


@end
