//
//  FHPostCell.h
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHPostCell : UITableViewCell
{
    IBOutlet UILabel *_textView;
    IBOutlet UILabel *_authorLabel;
    IBOutlet UILabel *_dateLabel;
    IBOutlet UIButton *_followersButton;
}
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSString *author;
@property (nonatomic, retain) NSString *postId;
@property (nonatomic, assign) BOOL followed;
@property (nonatomic, assign) NSInteger followers;

- (IBAction)pressedFollow:(id)sender;

@end
