//
//  FHLoginViewController.h
//  FAQ here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHLoginViewController : UIViewController <UITextFieldDelegate>
{
    IBOutlet UITextField *_textField;
}

- (IBAction)signIn:(id)sender;

@end
