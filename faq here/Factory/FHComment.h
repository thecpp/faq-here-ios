//
//  FHComment.h
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHPost.h"

@interface FHComment : FHPost
@property (nonatomic, retain) NSString *commentId;
@end
