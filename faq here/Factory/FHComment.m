//
//  FHComment.m
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHComment.h"

@implementation FHComment
@synthesize commentId;

- (void) dealloc
{
    self.commentId = nil;
    [super dealloc];
}

- (NSDictionary *) dictionary
{
    NSMutableDictionary *newDictionary = [NSMutableDictionary dictionaryWithDictionary:[super dictionary]];
    [newDictionary setObject:self.commentId forKey:@"commentId"];
    return newDictionary;
}

@end
