//
//  FHSettings.h
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSLocationManager.h"

@interface FHSettings : NSObject <SSLocationManagerDelegate>


@property (nonatomic, assign) float longtitude;
@property (nonatomic, assign) float latitude;
@property (nonatomic, retain) NSString *userName;
@property (nonatomic, assign) NSInteger userId;

+ (FHSettings *) settings;

@end
