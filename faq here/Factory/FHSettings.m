//
//  FHSettings.m
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHSettings.h"

static FHSettings *sSettings;
@implementation FHSettings
@synthesize userId, userName, longtitude, latitude;

+ (FHSettings *) settings
{
    if (!sSettings) 
    {
        sSettings = [FHSettings new];
    }
    return sSettings;
}

- (id) init
{
    self = [super init];
    if (self)
    {
        [[SSLocationManager sharedManager] addDelegate:self];
        [[SSLocationManager sharedManager] startUpdatingCurrentLocation];
    }
    return self;
}

- (void) dealloc
{
    [[SSLocationManager sharedManager] removeDelegate:self];
    self.userName = nil;
    [super dealloc];
}


- (NSString *) userName
{
    if (!userName) 
    {
        userName = [[NSUserDefaults standardUserDefaults] objectForKey:@"username"];
    }
    return userName;
}

- (NSInteger) userId
{
    if (!userId) 
    {
        userId = [[[NSUserDefaults standardUserDefaults] valueForKey:@"userid"] intValue];
    }
    return userId;
}

- (float) latitude
{
    if (!latitude) 
    {
        latitude = [[NSUserDefaults standardUserDefaults] floatForKey:@"latitude"];
    }
    return latitude;
}

- (float) longtitude
{
    if (!longtitude) 
    {
        longtitude = [[NSUserDefaults standardUserDefaults] floatForKey:@"longtitude"];
    }
    return longtitude;    
}

- (void) setUserId:(NSInteger)newUserId
{
    userId = newUserId;
    [[NSUserDefaults standardUserDefaults] setInteger:newUserId forKey:@"userid"];
}

- (void) setUserName:(NSString *)newUserName
{
    [userName release];
    userName = [newUserName retain];
    [[NSUserDefaults standardUserDefaults] setObject:newUserName forKey:@"username"];
}

- (void) ssLocationManager:(SSLocationManager *)locManager updatedCurrentLocation:(YahooPlaceData *)_currentLocation
{
//    slname = _currentLocation.state;
    self.latitude = [_currentLocation.latitude floatValue];
    self.longtitude = [_currentLocation.longitude floatValue];
    [[NSUserDefaults standardUserDefaults] setFloat:self.latitude forKey:@"latitude"];
    [[NSUserDefaults standardUserDefaults] setFloat:self.longtitude forKey:@"longtitude"];    
}

- (void) ssLocationManager:(SSLocationManager *)locManager didFailWithError:(NSError *)error
{
}


@end
