//
//  FHPost.h
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FHPost : NSObject
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSString *postId;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSString *author;

- (NSDictionary *) dictionary;

@end
