//
//  FHPost.m
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHPost.h"

@implementation FHPost
@synthesize author, date, postId, text;

- (void) dealloc
{
    self.author = nil;
    self.date = nil;
    self.postId = nil;
    self.text = nil;
    [super dealloc];
}

- (NSDictionary *) dictionary
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:self.author,@"author", date, [NSNumber numberWithInt:[date timeIntervalSince1970]], @"date", self.text, @"text",nil];
    return dictionary;
}

+ (FHPost *) postFromDictionary:(NSDictionary *)dictionary
{
    FHPost *post = [FHPost new];
    post.author = [dictionary objectForKey:@"author"];
    post.date = [NSDate dateWithTimeIntervalSince1970:[[dictionary objectForKey:@"date"] intValue]]; 
    post.text = [dictionary objectForKey:@"text"];
    post.postId = [dictionary objectForKey:@"postId"];
    return [post autorelease];
}

@end
