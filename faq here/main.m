//
//  main.m
//  faq here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FHAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FHAppDelegate class]));
    }
}
