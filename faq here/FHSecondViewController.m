//
//  FHSecondViewController.m
//  FAQ here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHSecondViewController.h"
#import "FHLoginViewController.h"
#import "FHNetwork.h"

@interface FHSecondViewController ()

@end

@implementation FHSecondViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"Ask", @"Ask");
        self.tabBarItem.image = [UIImage imageNamed:@"second"];
    }
    return self;
}
					
- (void) signOut
{
    UIViewController *loginVC = [FHLoginViewController new];
    
    [self.tabBarController presentModalViewController:loginVC animated:NO];
    [loginVC release];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Sign Out" style:UIBarButtonItemStyleBordered target:self action:@selector(signOut)] autorelease];

	// Do any additional setup after loading the view, typically from a nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)sent:(id)sender
{
    [_textField resignFirstResponder];
    [[FHNetwork sharedInstance] postQuestion:_textField.text delegate:self];
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (![touch.view isEqual:_textField]) 
    {
        [_textField resignFirstResponder];        
    }
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [self sent:nil];
    
    return YES;
}

- (void) postedWithError:(NSError *)error
{
    NSString *text = (!error)? @"Successfully posted" : @"Failed";
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:text message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [alert release];
}


@end
