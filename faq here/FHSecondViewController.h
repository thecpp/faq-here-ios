//
//  FHSecondViewController.h
//  FAQ here
//
//  Created by Victor Schepanovsky on 21.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FHNetwork.h"

@interface FHSecondViewController : UIViewController <UITextFieldDelegate, FHPostDelegate>
{
    IBOutlet UITextField *_textField;
    
}

- (IBAction)sent:(id)sender;

@end
