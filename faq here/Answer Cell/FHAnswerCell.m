//
//  FHAnswerCell.m
//  faq here
//
//  Created by Victor Schepanovsky on 22.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import "FHAnswerCell.h"
#import "FHSettings.h"
#import "FHNetwork.h"

@implementation FHAnswerCell
@synthesize text, rating, isCorrect, helpful,date, author, answerId, authorId, followers, postAuthorId;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [[[[NSBundle mainBundle] loadNibNamed:@"FHAnswerCell" owner:self options:nil] objectAtIndex:0] retain];
    if (self) 
    {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void) setDate:(NSDate *)newDate
{
    date = newDate;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    _dateLabel.text = [dateFormatter stringFromDate:newDate];
    [dateFormatter release];
}

- (void) setAuthor:(NSString *)authorText
{
    author = authorText;
    _authorLabel.text = authorText;
}

- (void) setText:(NSString *)textString
{
    text = textString;
    _textView.text = textString;
}

- (void) setHelpful:(NSInteger)newhelpful
{
    helpful = newhelpful;
    if (!helpful && _checkButton.hidden) 
    {
        _plusButton.hidden = NO;
        _minusButton.hidden = NO;
    }
    else 
    {
        _plusButton.hidden = YES;
//        _ratingLabel.hidden = YES;

        _minusButton.hidden = YES;
    }
}

- (void) setIsCorrect:(BOOL)newisCorrect
{
    isCorrect = newisCorrect;
    if (isCorrect) 
    {
        _plusButton.hidden = YES;
        _ratingLabel.hidden = YES;

        _minusButton.hidden = YES;
        _checkButton.hidden = NO;
        UIImage *img = [UIImage imageNamed:@"checkmark_on.png"];
        [_checkButton setImage:img forState:UIControlStateNormal];
        [_checkButton setImage:img forState:UIControlStateSelected];
        [_checkButton setImage:img forState:UIControlStateHighlighted];
    }
    else 
    {
        _checkButton.hidden = NO;
        UIImage *img = [UIImage imageNamed:@"checkmark_off.png"];
        [_checkButton setImage:img forState:UIControlStateNormal];
        [_checkButton setImage:img forState:UIControlStateSelected];
        [_checkButton setImage:img forState:UIControlStateHighlighted];        
    }
}

- (void) setRating:(NSInteger)newrating
{
    rating = newrating;
    _ratingLabel.text = [NSString stringWithFormat:@"%d", newrating];
}

- (IBAction)plus:(id)sender
{
    self.helpful = 1;
    self.rating++;
    [[FHNetwork sharedInstance] voteForAnswerWithId:[NSString stringWithFormat:@"%d", self.answerId]];
}

- (IBAction)minus:(id)sender
{
    self.rating--;
    self.helpful = -1;
    [[FHNetwork sharedInstance] unVoteForAnswerWithId:[NSString stringWithFormat:@"%d", self.answerId]];
}

- (void) setPostAuthorId:(NSInteger)newauthorId
{
    authorId = newauthorId;
    if (newauthorId == [FHSettings settings].userId || self.isCorrect) 
    {
        _minusButton.hidden = YES;
        _plusButton.hidden = YES;
        _ratingLabel.hidden = YES;

        _checkButton.hidden = NO;
    }
    else 
    {
        if (!helpful) 
        {
            _minusButton.hidden = NO;
            _plusButton.hidden = NO;
        }
        _checkButton.hidden = YES;
    }
}

- (void) markAsCorrect:(id)sender
{
    if ([FHSettings settings].userId != self.postAuthorId) 
    {
        return;
    }
    
    if (self.isCorrect) 
    {
        [[FHNetwork sharedInstance] unMarkAnswerAsRight:[NSString stringWithFormat:@"%d", self.answerId]];
    }
    else 
    {
        [[FHNetwork sharedInstance] markAnswerAsRight:[NSString stringWithFormat:@"%d", self.answerId]];
    }
    self.isCorrect = !isCorrect;
}



@end
