//
//  FHAnswerCell.h
//  faq here
//
//  Created by Victor Schepanovsky on 22.04.12.
//  Copyright (c) 2012 Kuadriga. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FHAnswerCell : UITableViewCell
{
    IBOutlet UILabel *_textView;
    IBOutlet UILabel *_authorLabel;
    IBOutlet UILabel *_dateLabel;
    IBOutlet UILabel *_ratingLabel;
    IBOutlet UIButton *_plusButton;
    IBOutlet UIButton *_minusButton;
    IBOutlet UIButton *_checkButton;
}
@property (nonatomic, retain) NSString *text;
@property (nonatomic, retain) NSDate *date;
@property (nonatomic, retain) NSString *author;
@property (nonatomic, assign) NSInteger answerId;
@property (nonatomic, assign) NSInteger followers;
@property (nonatomic, assign) NSInteger authorId;
@property (nonatomic, assign) NSInteger postAuthorId;
@property (nonatomic, assign) BOOL isCorrect;
@property (nonatomic, assign) NSInteger rating;
@property (nonatomic, assign) NSInteger helpful;
- (IBAction)markAsCorrect:(id)sender;
- (IBAction)plus:(id)sender;
- (IBAction)minus:(id)sender;

@end
